# WTF-Database #

This is a repository for a Debian package which installs a database for 2E0EOL/WTF and the original BSDGames WTF acronym program. 

### What is this repository for? ###

* Custom Database for WTF acronym program.
* Contains easy install Debian package.
* Contains source database if you prefer to manually copy file to user space.

### Link to WTF program this database is for ###

* [2E0EOL's C++ rewrite of BSD Games WTF.](https://hg.sr.ht/~m6kvm/wtf)
* Original BSD Games WTF: `aptitude install bsdgame`
### How do I Install? ###

* Download latest version from [/downloads](https://bitbucket.org/2E0PGS/wtf-database/downloads) and open with package manager.
* Alternatively do it from the command line:
* `sudo dpkg -i wtf-database_x.x.x.deb`
* xxx being the version you downloaded from [/downloads](https://bitbucket.org/2E0PGS/wtf-database/downloads).

### Building .deb package from source using dpkg-deb --build ###

* `git clone https://2E0PGS@bitbucket.org/2E0PGS/wtf-database.git`
* `cd wtf-database`
* `chmod +x EasyBuild.sh`
* `./EasyBuild.sh wtf-database x.x.x`
* x.x.x input a version number like 0.0.2

### Building for other OS (FreeBSD) ###

* `git clone https://2E0PGS@bitbucket.org/2E0PGS/wtf-database.git`
* `cd wtf-database`
* `sudo make install`

### Contribution guidelines ###

* Fork repo
* Submit pull request

### Who do I talk to? ###

* [2E0PGS](https://bitbucket.org/2E0PGS) or [2E0EOL](https://bitbucket.org/2E0EOL)