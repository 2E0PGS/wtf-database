#!/bin/bash

# EasyBuild script for creating a .deb from within a repository.
# Created by: Peter Stevenson 2E0PGS

if [ $# -eq 0 ]
    then
        echo "usage: ./EasyBuild <project_name> <version>"
        exit 1
fi

program_name=$1;
program_name_tmp="$program_name"_$2"";

echo "EasyBuild script for creating a .deb from within a repository."
echo "--------------------------------------------------------------"
cd ..
mkdir $program_name_tmp
mkdir -p $program_name_tmp/usr/local/share/wtf
cd $program_name
cp src/acronyms.txt ../$program_name_tmp/usr/local/share/wtf
cp -r debian/ ../$program_name_tmp/debian
mv ../$program_name_tmp/debian ../$program_name_tmp/DEBIAN
cd ..
dpkg-deb --build $program_name_tmp
echo "Cleaning up temporary files."
rm -r $program_name_tmp
echo "Checking associated files with .deb"
dpkg -c $program_name_tmp.deb
echo "Done..."